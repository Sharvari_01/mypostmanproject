# Mypostmanproject
Welcome to the My postmanproject! This collection contains a comprehensive set of API requests for interacting with the My API service. Whether you're a developer integrating with the My API, a tester validating its functionality, or an API enthusiast exploring its capabilities, this collection provides a convenient way to interact with the endpoints and understand their behavior.

## Getting started
1.Download Collection: Obtain the `REST_API.postman_collection.json` file from the repository.
2.Download environment: Obtain the `RespResEnv.postman_environment.json` file from the repository.

Those who want to use this structure can use the following software for implementation.
1.To elaborate : Postman (latest_version) {Link : https://www.postman.com/downloads/}
2.To implement : Newman {Link : https://support.postman.com/hc/en-us/articles/115003703325-How-to-install-Newman}


## Some of the GIT commands that were used 

1.To make a clone of repository 
Git clone : https://gitlab.com/Sharvari_01/mypostmanproject.git

2.To create a branch
git checkout -b <branchname>

3.To add the all files
git add *

4.To check Whether all the files are added or note
git status 

5.To push it in the repository
git push origin <branchname>

#Postman version which was used 
 v11.1.0

#Project status
The project is currently in the beta stage . It is actively maintained, with regular updates, bug fixes, and improvements. We welcome contributions from the community to help enhance the project further.
